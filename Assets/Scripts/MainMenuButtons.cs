﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MainMenuButtons : MonoBehaviour
{
    private void Start()
    {
        Time.timeScale = 1f;
        PlayerPrefs.SetInt("Difficulty", 2);
    }

    public void PlayButton(string scene) {SceneManager.LoadScene("Level01"); }

    public void SelectLevelButton(string SelectLevel) { SceneManager.LoadScene("SelectLevel"); }

    public void SettingsButton(string Settings) {SceneManager.LoadScene("Settings"); }

    public void Instructions(string instructions) { SceneManager.LoadScene("instructions"); }

    public void LevelOne(string scene) { SceneManager.LoadScene("Level01"); }

    public void LevelTwo(string scene) { SceneManager.LoadScene("Level02"); }

    public void BackToMainMenu(string scene) { SceneManager.LoadScene("MainMenu"); }

    public void EasyDifficulty() { PlayerPrefs.SetInt("Difficulty", 2); }

    public void MediumDifficulty() { PlayerPrefs.SetInt("Difficulty", 3); }

    public void HardDifficulty() { PlayerPrefs.SetInt("Difficulty", 4); }
}
