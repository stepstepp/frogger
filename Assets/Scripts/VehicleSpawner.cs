﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSpawner : MonoBehaviour
{
    public List<GameObject> vehicles = new List<GameObject>();
    public Transform spawnPos;
    public Transform despawnPos;
    public Transform roadPos;
    public float minSpawnTime;
    public float maxSpawnTime;
    public bool isRight;

    SoundManager soundManager;

    private void Awake()
    {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnVehicle());
    }

    private IEnumerator SpawnVehicle()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));

            if (roadPos.position.x >= 2 && roadPos.position.x <= 3)
            {
                GameObject go = Instantiate(vehicles[0], spawnPos.position, Quaternion.identity);
                if (!isRight)
                {
                    go.transform.Rotate(new Vector3(0, 180, 0));
                    soundManager.playSound(SoundManager.Sound.Vehicle);
                }
            }

            if (roadPos.position.x >=4 && roadPos.position.x <= 6)
            {
                GameObject go = Instantiate(vehicles[1], spawnPos.position, Quaternion.identity);
                if (!isRight)
                {
                    go.transform.Rotate(new Vector3(0, 180, 0));
                }
            }

            if (roadPos.position.x >= 8 && roadPos.position.x <= 12)
            {
                GameObject go = Instantiate(vehicles[2], spawnPos.position, Quaternion.identity);
                if (!isRight)
                {
                    go.transform.Rotate(new Vector3(0, 180, 0));
                }
            }

        }
        
    }
}
