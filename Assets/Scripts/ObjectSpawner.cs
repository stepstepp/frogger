
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{

    [SerializeField] private GameObject[] prefabs;
    [SerializeField] private bool leftSide;
    [SerializeField] private float minSpawn;
    [SerializeField] private float maxSpawn;
    [SerializeField] private float speed;

    SoundManager soundManager;
    GameController gameController;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnVehicle());

        if (Random.Range(0,2) == 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, -10);
            leftSide = true;
        } else
            {

                transform.position = new Vector3(transform.position.x, transform.position.y, 10);
            }
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator SpawnVehicle()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(2, 4));
            GameObject ob = Instantiate(prefabs[Random.Range(0,prefabs.Length)], gameObject.transform.position, Quaternion.identity);
            ob.GetComponent<Log>().setSpeed(speed);

            if (!leftSide)
                ob.transform.Rotate(new Vector3(0, 180, 0));
        }
    }
}

