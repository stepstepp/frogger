﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{

    SoundManager soundManager;

    private void Awake()
    {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        soundManager.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("volume");
    }

    private void Start()
    {
        soundManager.playSound(SoundManager.Sound.BGM);
    }
}
