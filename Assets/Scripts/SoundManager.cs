﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] audioClips;
  

    public enum Sound
    {
        BGM,
        Jump,
        Vehicle,
        Crash,
        Victory,
        GameOver,
        Splash,
        Drown,
    }


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
    }

    public void playSound(Sound sound)
    {
        switch (sound)
        {
            case Sound.BGM:
                audioSource.Stop();
                audioSource.clip = audioClips[0];
                audioSource.Play();
                break;
            case Sound.Jump:
                audioSource.PlayOneShot(audioClips[1]);
                break;
            case Sound.Vehicle:
                audioSource.PlayOneShot(audioClips[2]);
                break;
            case Sound.Crash:
                audioSource.PlayOneShot(audioClips[3]);
                break;
            case Sound.Splash:
                audioSource.PlayOneShot(audioClips[4]);
                break;
            case Sound.Drown:
                audioSource.PlayOneShot(audioClips[5]);
                break;
            case Sound.Victory:
                audioSource.Stop();
                audioSource.clip = audioClips[6];
                audioSource.Play();
                break;
            case Sound.GameOver:
                audioSource.Stop();
                audioSource.PlayOneShot(audioClips[7]);
                break;
        }
    }
}


